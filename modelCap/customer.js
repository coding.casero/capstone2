const mongoose = require("mongoose");

//Chose option 1 model

const customerSchema = new mongoose.Schema({
 	   name : {
        type : String,       
    },
    
       email : {
        type : String,       
    },
    
    password : {
        type : String,
    },

    isAdmin : {
        type : Boolean,
        default : false
    },

  
    orderedProduct : [
        {   
    	   products: [{
                    
                    productID:  {
                        type:String
                    },

                    productName: {
                        type:String
                    },

                    quantity: { 
                        type: Number,
                        default: 0
                    }    
            }],

            totalAmount : {
                        type : Number,
                        default : 0
            },

            purchasedOn : {
                        type : Date,
                        default : new Date()
            }
        }
    ]
})




module.exports = mongoose.model("Customer", customerSchema);



//options 2 