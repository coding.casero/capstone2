const mongoose = require("mongoose");

//Chose option 1 model

const productSchema = new mongoose.Schema(
{

    name : {
        type : String,
    },
    description : {
        type : String,
    },
    isActive : {
        type : Boolean,
        default : true
    },
    price: {
    	type: Number,
    	default: 0 
    },
    createdOn:{
    	type : Date,
        default : new Date()
    },
    userOrders : [
    	{      
            userId :{ 
            type:String
            },
            orderID: {
            	type:String
           	}
        }
    ]
})




module.exports = mongoose.model("Product", productSchema);