
const express = require("express");
const customerController = require("../controllerCap/customer");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();





//test for postman
router.get("/check", customerController.check);


//Routes
//Register a customer
router.post("/register", customerController.registerCustomer);

//Customer Authentication
router.post("/login", customerController.loginCustomer);

//Create Order
router.put("/order", verify, customerController.createOrder);

//Customer Details
router.get("/findcustomer", customerController.customerDetails);

//Customer Login Details
router.get("/details", verify, customerController.getProfile);

//set to Admin
//router.put("/:customerid/setadmin", verify, verrifyAdmin, customerController.setAdmin);

module.exports = router;