
const express = require("express");
const productController = require("../controllerCap/product");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();




//test for postman
router.get("/check", productController.check);


//Routes
//Add Product
router.post("/addproduct", verify, verifyAdmin, productController.addProduct)

//List All Products
router.get("/listproduct",  productController.listProduct)

//List Active Products
router.get("/listactiveproduct", verify, verifyAdmin, productController.listActiveProduct)

//Find one Product
router.get("/:productId", productController.findProduct)

//Update Product
router.put("/:productId/update", verify, verifyAdmin, productController.updateProduct)

//Archive Product
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct)

//Reactivate Product
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct)






module.exports = router;