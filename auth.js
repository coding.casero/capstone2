const jwt = require("jsonwebtoken");
const secret = "capstoneAPI";

module.exports.createAccessToken = (customer)=>{
	const data = {
		id: customer._id,
		email:customer.email,
		isAdmin: customer.isAdmin
	};
	return jwt.sign(data, secret, {});
}




module.exports.verify = (req,res,next)=>{
	console.log(req.headers.authorization)

	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth:"Access Denied(Failed to verify)"});
	}else{
		
		token = token.slice(7,token.length)

		jwt.verify(token, secret, function(err,decodedToken) {
		
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			}else{
				req.user = decodedToken
				next()
			}
		})
	}
}

module.exports.verifyAdmin = (req,res,next)=>{

	//verifyAdmin comes AFTER the verify middleware

	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth:"Failed",
			message:"Action Forbidden"
		})
	}
}