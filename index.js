const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


//Routes
const customerRoute = require("./routeCap/customer")
const productRoute = require("./routeCap/product")


const port = 4000


const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());




//Database Connection
	//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://codingcasero:J6TlL1lchUbQacsz@capstonecluster.lgklolo.mongodb.net/CapstoneDatabase",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)




	//prompt
let db = mongoose.connection;
db.on('error', console.error.bind(console,'Connection error'));
db.once('open', () => console.log('Connected to your MongoDB Atlas!'));

	
app.use("/customers", customerRoute);
app.use("/products", productRoute);


//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}


module.exports = {app,mongoose};
