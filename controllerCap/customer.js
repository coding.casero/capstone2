const Customer = require("../modelCap/customer")
const Product = require("../modelCap/product")
const bcrypt = require("bcrypt");
const auth = require("../auth");




module.exports.check = (req, res) =>{
	res.send("Hello World")
}

module.exports.checkEmailExists = (req, res) =>{
	return Customer.find({email: req.body.email}).then(result=>{
		console.log(result)
		if(result.length > 1) {
			return res.send(true); 
		} else {
			return res.send(false); 
		}
	})
}


module.exports.registerCustomer = (req, res) =>{

	Customer.findOne({ email: req.body.email })
        .then(existingCustomer => {
            if (existingCustomer) {
                return res.send(false);
            }


            const newCustomer = new Customer({
            	name: req.body.name,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10)
            });

            return newCustomer.save();
        })
        .then(() => {
            return res.send(true);
        })
        .catch(error => {
            console.error(error);
            return res.send(false);
        });
};


module.exports.loginCustomer = (req,res) => {
	
	return Customer.findOne({email: req.body.email}).then(result=>{
		
		if(result === null) {
			return res.send(false)
		} else {
			const passwordCheck = bcrypt.compareSync(req.body.password, result.password)
			if (passwordCheck) {
				return res.send({access: auth.createAccessToken(result)})
			} else {
				return res.send(false);
			}


		}
	})
	.catch(err=>res.send(err))
}


module.exports.createOrder = (req,res) =>{

	if (req.user.isAdmin === true) return res.send("Admin accounts are restricted from making orders")

	Customer.findOne({email: req.user.email}).then(result =>{

		let order = {
			productID: req.body.productID,
			productName: req.body.productName,
			quantity: req.body.quantity
		}
    	if (!result.orderedProduct[0]) {
        result.orderedProduct[0] = {};
      }

      if (!result.orderedProduct[0].products) {
        result.orderedProduct[0].products = [order];
      } else {
        result.orderedProduct[0].products.push(order);
      }
		result.save()
		return res.send(true)
	})
	.catch(err => res.send(err));
}
		

module.exports.customerDetails = (req,res) =>{
    return Customer.findOne({email: req.body.email}).then(customer => {
        	if (customer !== null){
        		return res.send(customer)
        	} else return res.send("No User Found") 
    	})
}

module.exports.getProfile = (req, res) => {

		return Customer.findById(req.user.id)
		.then(result => {
			result.password = "";
			return res.send(result);
		})
		.catch(err => res.send(err))
	};