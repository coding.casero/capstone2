const Product = require("../modelCap/product")
const bcrypt = require("bcrypt");
const auth = require("../auth");



//for checking
module.exports.check = (req, res) =>{
	res.send("Hello World")
}

//listing new products
module.exports.addProduct =(req, res) => {
    
    let newProduct = new Product({
        name : req.body.name,
        description : req.body.description,
        price: req.body.price
    });

    let checkCopy = (a) => {
        return Product.find(a).then(b =>{ 
            if (b.length > 0) return res.send("Action denied! Product already on Sale!")
        })
    }

    checkCopy({name: req.body.name})

    return newProduct.save().then((product, error) => {
        if (error) {
            return res.send(false);

 
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};





//getting all products
module.exports.listProduct =(req, res) => {
	Product.find({})
    .then((result) => {
      const productList = result.map((product) => ({
        id: product._id,
        name: product.name,
        description: product.description,
        price: product.price,
        isActive: product.isActive
      }));
      return res.json(productList);	})
}

module.exports.listActiveProduct =(req, res) => {
    Product.find({isActive:true})
    .then((result) => {
      const productList = result.map((product) => ({
        name: product.name,
        description: product.description,
        price: product.price,
        isActive: product.isActive
      }));
      return res.json(productList); })
}

module.exports.findProduct =(req, res) => {
    return Product.findById(req.params.productId).then(result=>{return res.send(result)})

}

module.exports.updateProduct = (req,res)=>{

    let updatedDetails = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }


    return Product.findByIdAndUpdate(req.params.productId, updatedDetails).then((product, error)=>{

        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}


module.exports.archiveProduct = (req,res) =>{


    return Product.findById(req.params.productId).then((product)=>{
        if (product.isActive) {
            product.isActive = false
            
     
            product.save()
            return res.send(product.isActive);
            };
        })
}

module.exports.activateProduct = (req,res) =>{


    return Product.findById(req.params.productId).then((product)=>{
        if (!product.isActive) {
            product.isActive = true
            

            product.save()
            return res.send(product.isActive);
            };
        })
}

