E-COMMERCE API DOCUMENTATION
by Sean Andrew Casero


~TEST ACCOUNTS:~
- Regular User:
     - email: "Don@gmail.com"
     - password: "Juan"

- Admin User:
    - email: admin@gmail.com
    - password: "checker"
    


~ROUTES:~
- User registration (POST)
	-  http://url//users/login
    - request body: 
        - email (string)
        - password (string)

- User authentication (POST)
	-  http://url//users/
    - request body: 
        - email (string)
        - password (string)

- Create Product (Admin only) (POST)
	-  http://url//products/addproduct
    - request body: 
        - name (string)
        - description (string)
        - price (number)

- Retrieve all products (Admin only) (GET)
	-  http://url//products/listproduct
    - request body: none

- Retrieve active products (Admin only) (GET)
    -  http://url//products/listactiveproduct
    - request body: none

- Retrieve one product (Admin only) (GET)
    -  http://url//products/:id (sampler ID)
    - request body: none

- Update Product details product (Admin only) (PUT)
    -  http://url//products/:id
    - request body: none

- Archive product (Admin only) (PUT)
    - http://url/products/:id/archive
    - request body: none

- Activate Product (Admin only) (PUT)
    -  http://url//:id/activate
    - request body: none

- Create Order (PUT)
    -  http://url//customers/order
    - request body: none
